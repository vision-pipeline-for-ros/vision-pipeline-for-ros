"""
Vision Pipeline for ROS
Copyright 2020 Vision Pipeline for ROS
Author: Fabian Kranewitter <fabiankranewitter@gmail.com>
SPDX-License-Identifier: MIT
"""


class AlgorithmTemplate(object):
    def get_io_type(self):
        raise NotImplementedError("subclasses must override get_io_type()!")

    def on_enable(self):
        raise NotImplementedError("subclasses must override on_enable()!")

    def on_disable(self):
        raise NotImplementedError("subclasses must override on_disable()!")

    def main(self, data):
        raise NotImplementedError("subclasses must override main()!")

    def on_config_change(self, conf):
        raise NotImplementedError(
            "subclasses must override on_config_change()!"
        )

    def on_additional_data_change(self, args):
        raise NotImplementedError(
            "subclasses must override on_additional_data_change()!"
        )
