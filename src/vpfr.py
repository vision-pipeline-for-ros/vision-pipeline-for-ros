"""
Vision Pipeline for ROS
Copyright 2020 Vision Pipeline for ROS
Author: Fabian Kranewitter <fabiankranewitter@gmail.com>
Author: Sebastian Bergner <sebastianbergner5892@gmail.com>
SPDX-License-Identifier: MIT
"""

import sys
import os
import rospy
import rosparam
import yaml
import rospkg
import importlib
import traceback
import psutil
from vpfr.srv import *
from vpfr.msg import *
from algorithm_data import AlgorithmData


class VisionPipeline:
    def __init__(
        self,
        pack_name,
        subscribe_channel,
        subscribe_channel_type,
        publish_channel,
        publish_channel_type,
        args=[],
    ):
        #  --config--  #
        self.__vpfr_name = "vpfr"
        self.__default_config_name = "default_config.yaml"
        self.__default_algorithm_info = "vpfr_algorithm.yaml"
        self.__config_dir = "config"
        self.__default_loa_config = "config.yaml"
        self.__param_name_loa = "algorithms"
        self.__algorithm_config_dir = "config"
        #  --end-config--  #
        self.__args = args
        #  remove file name
        self.__args.pop(0)
        #  remove sub channel
        self.__args.pop(0)
        #  remove pub channel
        self.__args.pop(0)
        #  remove log
        self.__args.pop(len(self.__args) - 1)
        #  remove
        self.__args.pop(len(self.__args) - 1)

        self.__subscribe_channel_type = subscribe_channel_type
        self.__publish_channel_type = publish_channel_type
        self.__subscribe_channel = subscribe_channel
        self.__publish_channel = publish_channel
        rospy.init_node(pack_name, anonymous=True)
        self.__vpfrinstance = rospy.get_name()
        #  remove node namespace
        self.__vpfrinstance = self.__vpfrinstance.replace("/", "")
        self.__vpfr_full_name = (
            self.__vpfr_name + "-" + self.__vpfrinstance + "-" + pack_name
        )
        self.__log_name = "[" + self.__vpfr_full_name + "]"
        self.__root_name = pack_name
        self.__rospack = rospkg.RosPack()
        self.__root_dir = self.__rospack.get_path(pack_name)
        rospy.loginfo(
            self.__log_name + " start '%s_%s' with rootdir '%s'",
            self.__vpfr_name,
            pack_name,
            self.__root_dir,
        )
        rospy.loginfo(
            self.__log_name + " subscribing to '%s' and publishing to '%s'",
            self.__subscribe_channel,
            self.__publish_channel,
        )
        #  init vars
        self.__algorithm_list = {}
        self.__algorithm = None
        self.__no_algorithm_warn = False
        self.__param_name_loa = (
            "/"
            + self.__vpfr_name
            + "/"
            + self.__root_name
            + "/"
            + self.__param_name_loa
        )
        #  create dir
        self._create_vision_pipline_structure()
        #  start loading algorithm
        self._scan_all_packages()
        self._callback_reload_loa(None)
        #  service / publisher / subscriber
        #  switch
        self.__service_set_publisher = rospy.Service(
            "/{}/{}/{}/set_publisher".format(
                self.__vpfr_name, self.__root_name, self.__vpfrinstance
            ),
            ChannelSet,
            self._callback_set_publisher,
        )
        self.__service_set_subscriber = rospy.Service(
            "/{}/{}/{}/set_subscriber".format(
                self.__vpfr_name, self.__root_name, self.__vpfrinstance
            ),
            ChannelSet,
            self._callback_set_subscriber,
        )
        self.__service_set_algorithm = rospy.Service(
            "/{}/{}/{}/set_algorithm".format(
                self.__vpfr_name, self.__root_name, self.__vpfrinstance
            ),
            AlgorithmSet,
            self._callback_set_algorithm,
        )
        self.__service_get_algorithms = rospy.Service(
            "/{}/{}/{}/get_algorithms".format(
                self.__vpfr_name, self.__root_name, self.__vpfrinstance
            ),
            AlgorithmInfoList,
            self._callback_get_algorithms,
        )
        self.__service_set_additional_data = rospy.Service(
            "/{}/{}/{}/set_additional_data".format(
                self.__vpfr_name, self.__root_name, self.__vpfrinstance
            ),
            AdditionalDataSet,
            self._callback_set_additional_data,
        )
        self.__service_get_additional_data = rospy.Service(
            "/{}/{}/{}/get_additional_data".format(
                self.__vpfr_name, self.__root_name, self.__vpfrinstance
            ),
            AdditionalDataRequest,
            self._callback_get_additional_data,
        )
        #  get current
        self.__service_get_current_algorithm = rospy.Service(
            "/{}/{}/{}/get_current_algorithm".format(
                self.__vpfr_name, self.__root_name, self.__vpfrinstance
            ),
            AlgorithmInfo,
            self._callback_get_current_algorithm,
        )
        self.__service_get_current_channel = rospy.Service(
            "/{}/{}/{}/get_current_channels".format(
                self.__vpfr_name, self.__root_name, self.__vpfrinstance
            ),
            ChannelRequest,
            self._callback_get_current_channels,
        )
        #  algorithm set config
        self.__service_set_algorithm_config = rospy.Service(
            "/{}/{}/{}/set_algorithm_config".format(
                self.__vpfr_name, self.__root_name, self.__vpfrinstance
            ),
            DataConfig,
            self._callback_set_algorithm_config,
        )
        #  list of algo config reload
        self.__service_reload_loa = rospy.Service(
            "/{}/{}/{}/reload_loa".format(
                self.__vpfr_name, self.__root_name, self.__vpfrinstance
            ),
            Request,
            self._callback_reload_loa,
        )
        self.__service_scan_algorithms = rospy.Service(
            "/{}/{}/{}/scan_algorithms".format(
                self.__vpfr_name, self.__root_name, self.__vpfrinstance
            ),
            Request,
            self._callback_scan_algorithms,
        )
        self.__service_get_loa_files = rospy.Service(
            "/{}/{}/{}/get_loa_files".format(
                self.__vpfr_name, self.__root_name, self.__vpfrinstance
            ),
            ConfigLoaList,
            self._callback_get_loa_files,
        )
        #  load and save from param server (loa=list of algorithm)
        self.__service_copy_loa_from_paramserver_to_config = rospy.Service(
            "/{}/{}/{}/copy_loa_from_paramserver_to_config".format(
                self.__vpfr_name, self.__root_name, self.__vpfrinstance
            ),
            ConfigLoaSet,
            self._callback_copy_loa_from_paramserver_to_config,
        )
        self.__service_copy_loa_from_config_to_paramserver = rospy.Service(
            "/{}/{}/{}/copy_loa_from_config_to_paramserver".format(
                self.__vpfr_name, self.__root_name, self.__vpfrinstance
            ),
            ConfigLoaSet,
            self._callback_copy_loa_from_config_to_paramserver,
        )
        self.__pub_time_per_request = rospy.Publisher(
            "/{}/{}/{}/analytic".format(
                self.__vpfr_name, self.__root_name, self.__vpfrinstance
            ),
            PerformanceMsg,
            queue_size=10,
        )
        self.__pub = rospy.Publisher(
            publish_channel, publish_channel_type, queue_size=10
        )
        self.__subscriber = rospy.Subscriber(
            subscribe_channel, subscribe_channel_type, self._callback_channel
        )
        #  start ros
        rospy.loginfo(self.__log_name + " '%s' start", self.__vpfr_full_name)
        rospy.spin()

    def _callback_set_additional_data(self, request):
        rospy.loginfo(self.__log_name + " change additional data")
        self.__args = request.values
        if self.__algorithm is not None:
            self.__algorithm.set_additional_data(self.__args)
        return AdditionalDataSetResponse(True, "done")

    def _callback_get_additional_data(self, request):
        return AdditionalDataRequestResponse(self.__args, True, "done")

    def _callback_set_subscriber(self, request):
        subscribe_channel = request.channel_name
        if subscribe_channel is None:
            return ChannelSetResponse(False, "subscribe_channel is none")
        if self.__subscriber is not None:
            rospy.loginfo(self.__log_name + " unregister old subscriber")
            self.__subscriber.unregister()
        rospy.loginfo(self.__log_name + " register new subscriber")
        self.__subscribe_channel = subscribe_channel
        rospy.loginfo(
            self.__log_name + " change subscriberchannel to '%s'",
            self.__subscribe_channel,
        )
        self.__subscriber = rospy.Subscriber(
            self.__subscribe_channel,
            self.__subscribe_channel_type,
            self._callback_channel,
        )
        rospy.loginfo(self.__log_name + " done")
        return ChannelSetResponse(True, "done")

    def _callback_set_publisher(self, request):
        publish_channel = request.channel_name
        if publish_channel is None:
            return ChannelSetResponse(False, "publish_channel is none")
        if self.__pub is not None:
            rospy.loginfo(self.__log_name + " unregister old publisher")
            self.__pub.unregister()
        rospy.loginfo(self.__log_name + " register new publisher")
        self.__publish_channel = publish_channel
        rospy.loginfo(
            self.__log_name + " change publish_channel to '%s'",
            self.__publish_channel,
        )
        self.__pub = rospy.Publisher(
            self.__publish_channel, self.__publish_channel_type, queue_size=10
        )
        rospy.loginfo(self.__log_name + " done")
        return ChannelSetResponse(True, "done")

    def _callback_scan_algorithms(self, request):
        rospy.loginfo(self.__log_name + " scan for new algorithms")
        rospy.loginfo(
            self.__log_name + " disable old algorithm list and clear it"
        )
        self.__algorithm = None
        for algorithm_obj in self.__algorithm_list.values():
            if algorithm_obj.has_instance():
                algorithm_obj.unload()
        self.__algorithm_list = {}
        rospy.loginfo(self.__log_name + " scan")
        self._scan_all_packages()
        rospy.loginfo(
            self.__log_name + " read and load all allowed algorithms"
        )
        enable_name_list = self._get_param_server_info()
        self._load_algorithm_class(enable_name_list)
        self._set_all_default_config()
        rospy.loginfo(self.__log_name + " done")
        return RequestResponse(True, "done")

    def _callback_reload_loa(self, request):
        rospy.loginfo(self.__log_name + " disable old algorithm list")
        self.__algorithm = None
        for algorithm_obj in self.__algorithm_list.values():
            if algorithm_obj.has_instance():
                algorithm_obj.unload()
        rospy.loginfo(
            self.__log_name + " read and load all allowed algorithms"
        )
        enable_name_list = self._get_param_server_info()
        self._load_algorithm_class(enable_name_list)
        self._set_all_default_config()
        rospy.loginfo(self.__log_name + " done")
        return RequestResponse(True, "done")

    def _callback_copy_loa_from_paramserver_to_config(self, config):
        if ".yaml" in str(config.config_name):
            file_name = (
                self.__root_dir
                + "/"
                + self.__config_dir
                + "/"
                + config.config_name
            )
            if not os.path.exists(file_name):
                try:
                    rosparam.dump_params(
                        file_name,
                        ("/{}/{}/".format(self.__vpfr_name, self.__root_name)),
                    )
                    return ConfigLoaSetResponse(True, "done")
                except IOError:
                    return ConfigLoaSetResponse(
                        False,
                        "could not write to file '%s'",
                        traceback.format_exc(),
                    )
            else:
                return ConfigLoaSetResponse(
                    False, "configuration file already exists"
                )
        return ConfigLoaSetResponse(
            False, "wrong format for config name, must contain '.yaml'"
        )

    def _callback_copy_loa_from_config_to_paramserver(self, request):
        if rospy.has_param(self.__param_name_loa):
            rospy.delete_param(self.__param_name_loa)
            state, error = self._set_param_server_from_file(
                request.config_name
            )
            return ConfigLoaSetResponse(
                state, "done" if state else "fail : " + error
            )

    def _callback_get_algorithms(self, request):
        all_algorithms = []
        for algorithm in self.__algorithm_list.values():
            if not algorithm.is_fail():
                config_files = []

                for root, directory, files in os.walk(
                    algorithm.get_directory()
                    + "/"
                    + self.__algorithm_config_dir
                    + "/"
                ):
                    for file in files:
                        if ".yaml" in file:
                            current = False
                            if file == algorithm.get_current_config():
                                current = True
                            config_files.append(
                                ConfigInfoMsg(
                                    file, os.path.join(root, file), current
                                )
                            )

                current_status = algorithm == self.__algorithm
                all_algorithms.append(
                    AlgorithmInfoMsg(
                        algorithm.get_name(),
                        algorithm.get_version(),
                        algorithm.get_authors(),
                        algorithm.get_license(),
                        algorithm.get_class_name(),
                        algorithm.get_main_file(),
                        self.__root_name,
                        config_files,
                        algorithm.is_enabled(),
                        current_status,
                    )
                )
        return AlgorithmInfoListResponse(all_algorithms, True, "done")

    def _callback_get_loa_files(self, request):
        loa_files = []
        for root, directory, files in os.walk(
            self.__root_dir + "/" + self.__config_dir
        ):
            for yfile in files:
                if ".yaml" in yfile:
                    loa_files.append(
                        ConfigLoaMsg(yfile, os.path.join(root, yfile))
                    )
        return ConfigLoaListResponse(loa_files, True, "done")

    def _callback_get_current_algorithm(self, request):
        if self.__algorithm is None:
            return AlgorithmInfoResponse(None, False, "no algorithm is set")
        else:
            config_files = []
            for root, directory, files in os.walk(
                self.__algorithm.get_directory()
                + "/"
                + self.__algorithm_config_dir
                + "/"
            ):
                for yfile in files:
                    if ".yaml" in yfile:
                        current = False
                        if yfile == self.__algorithm.get_current_config():
                            current = True
                        config_files.append(
                            ConfigInfoMsg(
                                yfile, os.path.join(root, yfile), current
                            )
                        )
            return AlgorithmInfoResponse(
                AlgorithmInfoMsg(
                    self.__algorithm.get_name(),
                    self.__algorithm.get_version(),
                    self.__algorithm.get_authors(),
                    self.__algorithm.get_license(),
                    self.__algorithm.get_class_name(),
                    self.__algorithm.get_main_file(),
                    self.__root_name,
                    config_files,
                    self.__algorithm.is_enabled(),
                    True,
                ),
                True,
                "done",
            )

    def _callback_get_current_channels(self, request):
        if self.__publish_channel is None:
            return ChannelRequestResponse(
                None, None, False, "no publish_channel is set"
            )
        elif self.__subscribe_channel is None:
            return ChannelRequestResponse(
                None, None, False, "no subscribe_channel is set"
            )
        else:
            return ChannelRequestResponse(
                self.__subscribe_channel,
                self.__publish_channel,
                self.__subscribe_channel_type.__name__,
                self.__publish_channel_type.__name__,
                True,
                "done",
            )

    def _callback_set_algorithm_config(self, data):
        state = self._algorithm_set_config(
            data.algorithm_name, data.config_name
        )
        return DataConfigResponse(state, "done" if state else "fail")

    def _callback_channel(self, msg):
        if self.__algorithm is None or not self.__algorithm.is_enabled():
            if self.__no_algorithm_warn is False:
                self.__no_algorithm_warn = True
                rospy.logwarn(self.__log_name + " no algorithm is set")
            return
        try:
            self.__no_algorithm_warn = False
            start_time = rospy.get_rostime()
            output = self.__algorithm.get_instance().main(msg)
            stop_time = rospy.get_rostime()
            self.__pub.publish(output)
            cpu_percent = psutil.cpu_percent(interval=1)
            ram_data = psutil.virtual_memory().percent
            time = (stop_time - start_time).nsecs
            self.__pub_time_per_request.publish(
                PerformanceMsg(time, cpu_percent, ram_data)
            )
        except Exception as err:
            rospy.logerr(
                self.__log_name + " algorithm '%s' raised an exeption : '%s'",
                self.__algorithm.get_name(),
                traceback.format_exc(),
            )

    def _callback_set_algorithm(self, request):
        rospy.loginfo(
            self.__log_name + " switch to '%s'", request.algorithm_name
        )
        algorithm = self.__algorithm_list.get(request.algorithm_name)
        if algorithm is None:
            rospy.loginfo(
                self.__log_name
                + " '%s' not found, please change to another algorithm!",
                request.algorithm_name,
            )
            return AlgorithmSetResponse(False, "algorithm not found")
        if not algorithm.has_instance():
            rospy.loginfo(
                self.__log_name
                + " '%s' has not instance (eg not supported), please change to another algorithm!",
                algorithm.get_name(),
            )
            return AlgorithmSetResponse(False, "algorithm not supported")
        if not algorithm.is_enabled():
            rospy.loginfo(
                self.__log_name
                + " '%s' is not enabled, please change to another algorithm!",
                algorithm.get_name(),
            )
            return AlgorithmSetResponse(False, "algorithm not enabled")
        if algorithm.is_fail():
            rospy.loginfo(
                self.__log_name
                + " '%s' algorithm loading failed, please change to another algorithm!",
                algorithm.get_name(),
            )
            return AlgorithmSetResponse(False, "algorithm loading failed")
        self.__algorithm = algorithm
        return AlgorithmSetResponse(True, "done")

    def _algorithm_set_config(self, algorithm_name, config_file):
        rospy.loginfo(
            self.__log_name + " load '%s' config for '%s'",
            config_file,
            algorithm_name,
        )
        algorithm = self.__algorithm_list.get(algorithm_name)
        if algorithm is not None:
            if os.path.isfile(
                (algorithm.get_directory() + "/config/" + config_file)
            ):
                documents = self._get_yaml_data(
                    (algorithm.get_directory() + "/config/" + config_file)
                )
                if type(documents) == dict:
                    try:
                        if algorithm.is_enabled():
                            algorithm.set_current_config(
                                config_file, documents
                            )
                            return True
                        else:
                            rospy.logwarn(
                                self.__log_name + " '%s' class not enabled",
                                algorithm.get_name(),
                            )
                            return False
                    except Exception as err:
                        rospy.logerr(
                            self.__log_name
                            + " algorithm '%s' raised an exeption : '%s'",
                            algorithm.get_name(),
                            traceback.format_exc(),
                        )
                        return False
                else:
                    rospy.logwarn(
                        self.__log_name + " can not decode '%s' config",
                        config_file,
                    )
                    return False
            else:
                rospy.logwarn(
                    self.__log_name + " can not find '%s' in '%s'",
                    algorithm.get_directory() + "config/",
                    config_file,
                )
                return False
        else:
            rospy.logwarn(
                self.__log_name + " can not find '%s' algorithm",
                algorithm_name,
            )
            return False

    def _create_dir(self, path):
        try:
            if os.path.isdir(path):
                rospy.loginfo(
                    self.__log_name + " directory '%s' already exists", path
                )
            else:
                os.mkdir(path)
                rospy.loginfo(
                    self.__log_name
                    + " successfully created the directory '%s' ",
                    path,
                )
        except OSError as err:
            rospy.logerr(
                self.__log_name
                + " creation of the directory '%s' failed : '%s'",
                path,
                traceback.format_exc(),
            )

    def _create_vision_pipline_structure(self):
        self._create_dir(self.__root_dir + "/" + self.__config_dir + "/")
        self._create_file_if_not_exist(
            self.__root_dir
            + "/"
            + self.__config_dir
            + "/"
            + self.__default_loa_config,
            "algorithms: ",
        )

    def _create_file_if_not_exist(self, path, data):
        if not os.path.exists(path):
            f = open(path, "w")
            f.write(data)
            f.close()
            rospy.loginfo(self.__log_name + " create file '%s'", path)
        else:
            rospy.loginfo(self.__log_name + " file '%s' already exist", path)

    def _scan_all_packages(self):
        ros_packages = self.__rospack.list()
        for package in ros_packages:
            directory = self.__rospack.get_path(package)
            rospy.logdebug(self.__log_name + " check '%s' dir", directory)
            if os.path.isfile(directory + "/" + self.__default_algorithm_info):
                rospy.loginfo(
                    self.__log_name + " found '%s' in directory '%s'",
                    self.__default_algorithm_info,
                    directory,
                )
                self._extract_algorithm_info(directory, package)

    def _extract_algorithm_info(self, algorithm_directory, package):
        algorithm_yaml = self._get_yaml_data(
            algorithm_directory + "/" + self.__default_algorithm_info
        )
        manifest = self.__rospack.get_manifest(package)
        if algorithm_yaml is not None:
            algorithm_name = package
            algorithm_version = manifest.version
            algorithm_license = manifest.license
            algorithm_authors = manifest.author
            algorithm_main_file = algorithm_yaml["mainfile"]
            algorithm_main_class = algorithm_yaml["mainclass"]
            if algorithm_name is None:
                rospy.logwarn(
                    self.__log_name + " no name was found in '%s'",
                    manifest.file_name,
                )
                return
            if algorithm_version is None:
                rospy.logwarn(
                    self.__log_name + " no version was found in '%s'",
                    manifest.file_name,
                )
            if algorithm_authors is None:
                rospy.logwarn(
                    self.__log_name + " no authors was found in '%s'",
                    manifest.file_name,
                )
            if algorithm_license is None:
                rospy.logwarn(
                    self.__log_name + " no license was found in '%s'",
                    manifest.file_name,
                )
            if algorithm_main_file is None or algorithm_main_class is None:
                rospy.logwarn(
                    self.__log_name
                    + " no main was found in %s in directory '%s'",
                    self.__default_algorithm_info,
                    algorithm_directory,
                )
                return
            rospy.loginfo(
                self.__log_name
                + " the data for the algorithm '%s' was successfully extracted",
                algorithm_name,
            )
            self._import_algorithm(
                algorithm_name,
                algorithm_directory,
                algorithm_version,
                algorithm_license,
                algorithm_authors,
                algorithm_main_file,
                algorithm_main_class,
            )
        else:
            rospy.logwarn(
                self.__log_name + " wrong '%s' in directory '%s' found",
                self.__default_algorithm_info,
                algorithm_directory,
            )

    def _import_algorithm(
        self,
        algorithm_name,
        algorithm_directory,
        algorithm_version,
        algorithm_license,
        algorithm_authors,
        algorithm_main_file,
        algorithm_main_class,
    ):
        if os.path.isfile(algorithm_directory + "/src/" + algorithm_main_file):
            if algorithm_main_file.endswith(".py"):
                algorithm_data = AlgorithmData(
                    algorithm_name,
                    algorithm_directory,
                    algorithm_version,
                    algorithm_license,
                    algorithm_authors,
                    algorithm_main_file,
                    algorithm_main_class,
                )
                self.__algorithm_list[
                    algorithm_data.get_name()
                ] = algorithm_data
                rospy.loginfo(
                    self.__log_name + " add %s algorithm data to the list",
                    algorithm_data.get_name(),
                )
            else:
                rospy.logwarn(
                    self.__log_name
                    + " only algorithms with the end '.py' are supported"
                )
        else:
            rospy.logwarn(
                self.__log_name + " no '%s' file was found in directory '%s'",
                algorithm_main_file,
                algorithm_directory + "/src/",
            )

    def _get_yaml_data(self, yaml_file):
        with open(yaml_file, "r") as file:
            documents = yaml.load(file, Loader=yaml.SafeLoader)
            if documents is None:
                rospy.logwarn(
                    self.__log_name + " %s has no entries", yaml_file
                )
                return None
            else:
                return documents

    def _set_param_server_from_file(self, file_name):
        rospy.loginfo(
            self.__log_name + " set param server from file '%s'", file_name
        )
        try:
            if not rospy.has_param(self.__param_name_loa):
                data = None
                config_yaml = rosparam.load_file(
                    "{}/{}/{}".format(
                        self.__root_dir, self.__config_dir, file_name
                    )
                )
                for array in config_yaml:
                    for entry in array:
                        if entry != "/":
                            data = entry["algorithms"]
                if data is not None:
                    rospy.set_param(self.__param_name_loa, data)
                else:
                    rospy.logwarn(self.__log_name + " config has no data")
                    return False, "config has no data"
            return True, None
        except Exception as err:
            rospy.logwarn(
                self.__log_name
                + " copy to paramserver raise an exeption: '%s'",
                traceback.format_exc(),
            )
            return False, str(err)

    def _load_algorithm_class(self, algorithms_yaml):
        if algorithms_yaml is not None:
            for algorithm in self.__algorithm_list.values():
                if algorithm is not None and not algorithm.is_enabled():
                    if algorithm.get_main_file().endswith(".py"):
                        rospy.loginfo(
                            self.__log_name + " load '%s' algorithm",
                            algorithm.get_name(),
                        )
                        try:
                            algorithm.load()
                            if algorithm.check_if_supported(
                                self.__subscribe_channel_type,
                                self.__publish_channel_type,
                            ):
                                rospy.loginfo(
                                    self.__log_name
                                    + " found supported algorithm '%s' for this type of vpfr",
                                    algorithm.get_name(),
                                )
                                enable = False
                                rospy.loginfo(
                                    self.__log_name
                                    + " check if the algorithm '%s' is enabled on the param server",
                                    algorithm.get_name(),
                                )
                                for enable_algorithm_name in algorithms_yaml:
                                    if (
                                        enable_algorithm_name
                                        == algorithm.get_name()
                                    ):
                                        enable = True
                                algorithm.set_fail_loading(False)
                                if enable:
                                    algorithm.set_additional_data(self.__args)
                                    algorithm.enable()
                                    rospy.loginfo(
                                        self.__log_name
                                        + " algorithm '%s' enabled",
                                        algorithm.get_name(),
                                    )
                                else:
                                    algorithm.unload()
                                    rospy.loginfo(
                                        self.__log_name
                                        + " algorithm '%s' not enabled on the param server",
                                        algorithm.get_name(),
                                    )
                            else:
                                algorithm.set_fail_loading(True)
                                algorithm.unload()
                                rospy.logdebug(
                                    self.__log_name
                                    + " algorithm '%s' is not supported for this type of vpfr",
                                    algorithm.get_name(),
                                )
                        except Exception as err:
                            algorithm.unload()
                            algorithm.set_fail_loading(True)
                            rospy.logwarn(
                                self.__log_name
                                + " init algorithm '%s' raised an exeption: '%s'",
                                algorithm.get_name(),
                                traceback.format_exc(),
                            )
                    else:
                        algorithm.set_fail_loading(True)
                        rospy.logwarn(
                            self.__log_name
                            + " no supported filetype (only .py (python) files)"
                        )
        else:
            rospy.logwarn(self.__log_name + " loa has no entries")

    def _set_all_default_config(self):
        for algorithm in self.__algorithm_list.values():
            if algorithm.is_enabled():
                self._algorithm_set_config(
                    algorithm.get_name(), self.__default_config_name
                )

    def _get_param_server_info(self):
        self._set_param_server_from_file(self.__default_loa_config)
        if rospy.has_param(self.__param_name_loa):
            algorithms_yaml = rosparam.get_param(self.__param_name_loa)
            if algorithms_yaml is None:
                rospy.logwarn(self.__log_name + " rosparam has no entries")
            return algorithms_yaml
        else:
            rospy.logwarn(self.__log_name + " rosparam has no entries")
            return None
