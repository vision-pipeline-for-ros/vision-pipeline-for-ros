"""
Vision Pipeline for ROS
Copyright 2020 Vision Pipeline for ROS
Author: Fabian Kranewitter <fabiankranewitter@gmail.com>
SPDX-License-Identifier: MIT
"""

import sys
import rospy


class AlgorithmData:
    def __init__(
        self, name, directory, version, license, authors, main, mainclass
    ):
        self.__name = name
        self.__directory = directory
        self.__version = version
        self.__main = main
        self.__authors = authors
        self.__license = license
        self.__mainclass = mainclass
        self.__instance = None
        self.__enabled = False
        self.__fail = True
        self.__current_config = None
        self.__additional_data = None

    def set_fail_loading(self, fail):
        self.__fail = fail

    def is_fail(self):
        return self.__fail

    def get_name(self):
        return self.__name

    def get_version(self):
        return self.__version

    def get_license(self):
        return self.__license

    def get_authors(self):
        return self.__authors

    def get_directory(self):
        return self.__directory

    def get_main_file(self):
        return self.__main

    def get_class_name(self):
        return self.__mainclass

    def is_enabled(self):
        return self.__enabled and self.has_instance()

    def get_instance(self):
        return self.__instance

    def has_instance(self):
        return self.__instance is not None

    def set_current_config(self, config_name, data):
        self.__instance.on_config_change(data)
        self.__current_config = config_name

    def get_current_config(self):
        return self.__current_config

    def load(self):
        if not self.has_instance():
            sys.path.append(self.__directory + "/src/")
            self.__instance = self._load_python_class(
                self.__main.replace(".py", ""), self.__mainclass
            )
        else:
            raise ImportError("has instance")

    def check_if_supported(self, subscribechannel_type, publishchannel_type):
        if self.has_instance():
            input_type, output_type = self.__instance.get_io_type()
            self.input_type = input_type
            self.output_type = output_type
            if self.input_type != subscribechannel_type:
                return False
            if self.output_type != publishchannel_type:
                return False
            return True
        return True

    def set_additional_data(self, args):
        self.__instance.on_additional_data_change(args)
        self.__additional_data = args

    def get_additional_data(self):
        return self.__additional_data

    def enable(self):
        if self.has_instance():
            self.__instance.on_enable()
            self.__enabled = True
        else:
            raise Exception("has instance")

    def _load_python_class(self, module_name, class_name):
        if not self.is_enabled():
            module = __import__(module_name)
            my_class = getattr(module, class_name)
            instance = my_class()
            return instance
        else:
            return None

    def unload(self):
        if self.has_instance():
            if self.is_enabled():
                self.__instance.on_disable()
                self.__enabled = False
            self.__instance = None
            sys.path.remove(self.__directory + "/src/")
